﻿using System;

namespace Projekt_Na_Zaliczenie
{
    public static class FigureField
    {
        private enum Select
        {
            Square = 1,
            Rectangle,
            Trapezoid,
            Cube,
            Cylinder,
            Cone,
            Exit
        }

        public static void Menu()
        {
            ShowMenuList();
            SelectOption();
        }

        private static void ShowMenuList()
        {
            Console.WriteLine("Choose what you want to calculate: ");
            Console.WriteLine("1. Square field");
            Console.WriteLine("2. Rectangle field");
            Console.WriteLine("3. Trapezoid field");
            Console.WriteLine("4. Cube volume and perimeter");
            Console.WriteLine("5. Cylinder volume and perimeter");
            Console.WriteLine("6. Cone volume and perimeter");
            Console.WriteLine("7. Back to menu");
        }

        private static void SelectOption()
        {
            Select option = 0;

            Console.Write("Choice: ");
            try
            {
                option = (Select)int.Parse(Console.ReadLine());
            }
            catch
            {
                Program.ShowError();
                SelectOption();
            }

            switch (option)
            {
                case Select.Square:
                    Console.Clear();
                    SquareField();
                    break;
                case Select.Rectangle:
                    Console.Clear();
                    RectangleField();
                    break;
                case Select.Trapezoid:
                    Console.Clear();
                    TrapezoidField();
                    break;
                case Select.Cube:
                    Console.Clear();
                    CubeField();
                    break;
                case Select.Cylinder:
                    Console.Clear();
                    CylinderField();
                    break;
                case Select.Cone:
                    Console.Clear();
                    ConeField();
                    break;
                case Select.Exit:
                    Program.ReturnToMenu();
                    break;
                default:
                    Program.ShowError();
                    SelectOption();
                    break;
            }
            NextCalculate();
        }

        private static void SquareField()
        {
            Console.Write("Give base value: ");
            GetFigurePoint.GetA();

            Square square = new Square(GetFigurePoint.a);
            Console.WriteLine("Square field is {0} \n", square.CalculateField());
        }

        private static void RectangleField()
        {
            Console.Write("Give first value: ");
            GetFigurePoint.GetA();

            Console.Write("Give second value: ");
            GetFigurePoint.GetB();

            Rectangle rectangle = new Rectangle(GetFigurePoint.a, GetFigurePoint.b);
            Console.WriteLine("Square field is {0} \n", rectangle.CalculateField());
        }

        private static void TrapezoidField()
        {
            Console.Write("Give first base value: ");
            GetFigurePoint.GetA();

            Console.Write("Give second base value: ");
            GetFigurePoint.GetB();

            Console.Write("Give height value: ");
            GetFigurePoint.GetH();

            Trapezoid trapezoid = new Trapezoid(GetFigurePoint.a, GetFigurePoint.b, GetFigurePoint.h);
            Console.WriteLine("Trapezoid field is {0} \n", trapezoid.CalculateField());
        }

        private static void CubeField()
        {
            Console.Write("Give base value: ");
            GetFigurePoint.GetA();

            Cube cube = new Cube(GetFigurePoint.a);
            Console.WriteLine("Cube volume is {0}, and perimeter is {1}", cube.CalculateVolume(), cube.CalculatePerimeter());
        }

        private static void CylinderField()
        {
            Console.Write("Give radius value: ");
            GetFigurePoint.GetR();

            Console.Write("Give height value: ");
            GetFigurePoint.GetH();

            Cylinder cylinder = new Cylinder(GetFigurePoint.r, GetFigurePoint.h);
            Console.WriteLine("Cylinder volume is {0}, and perimeter is {1}", cylinder.CalculateVolume(), cylinder.CalculatePerimeter());
        }

        private static void ConeField()
        {
            Console.Write("Give radius value: ");
            GetFigurePoint.GetR();

            Console.Write("Give height value: ");
            GetFigurePoint.GetH();

            Console.Write("Give side length: ");
            GetFigurePoint.GetA();

            Cone cone = new Cone(GetFigurePoint.a, GetFigurePoint.r, GetFigurePoint.h);
            Console.WriteLine("Cone volume is {0}, and perimeter is {1}", cone.CalculateVolume(), cone.CalculatePerimeter());
        }

        private static void NextCalculate()
        {
            string optionYesNo = "";

            Console.WriteLine("<Y/M> Do you want to calculate another figure <Y> or back to menu? <M>");
            try
            {
                optionYesNo = Console.ReadLine();
            }
            catch
            {
                Program.ShowError();
                NextCalculate();
            }

            switch (optionYesNo)
            {
                case "Y":
                case "y":
                    Console.Clear();
                    Menu();
                    break;
                case "M":
                case "m":
                    Program.ReturnToMenu();
                    break;
                default:
                    Console.WriteLine("\nSAY Y OR M!!!");
                    NextCalculate();
                    break;
            }
        }
    }
}