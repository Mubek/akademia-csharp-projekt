﻿namespace Projekt_Na_Zaliczenie
{
    public class Cone : Abstract3DFigure, ICalculate3D
    {
        public Cone(double a, double r, double h) : base(a, r, h)
        {
        }

        public double CalculateVolume()
        {
            return (1.0 / 3) * (r * r) * h;
        }

        public double CalculatePerimeter()
        {
            return pi * r * a + pi * (r * r);
        }
    }
}