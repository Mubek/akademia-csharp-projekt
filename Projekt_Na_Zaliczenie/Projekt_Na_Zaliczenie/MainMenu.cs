﻿using System;

namespace Projekt_Na_Zaliczenie
{
    public static class MainMenu
    {
        private enum Select
        {
            Calculator = 1,
            CalculatorBmi,
            FastCountingFigures,
            DatabaseFigures,
            Exit
        }

        public static void Menu()
        {
            ShowMenuList();
            SelectOption();
        }

        private static void ShowMenuList()
        {
            Console.WriteLine("Welcome in Mini Universal Calculator!");
            Console.WriteLine("Press a number for action:");
            Console.WriteLine("1. Calculator");
            Console.WriteLine("2. Body mass index calculator (BMI)");
            Console.WriteLine("3. Fast counting figure field");
            Console.WriteLine("4. Database of figures");
            Console.WriteLine("5. Exit");
        }

        private static void SelectOption()
        {
            Select option = 0;

            Console.Write("Choice: ");
            try
            {
                option = (Select)int.Parse(Console.ReadLine());
            }
            catch
            {
                Program.ShowError();
                SelectOption();
            }

            switch (option)
            {
                case Select.Calculator:
                    Console.Clear();
                    Calculator.Menu();
                    break;
                case Select.CalculatorBmi:
                    Console.Clear();
                    CalculatorBMI.Menu();
                    break;
                case Select.FastCountingFigures:
                    Console.Clear();
                    FigureField.Menu();
                    break;
                case Select.DatabaseFigures:
                    Console.Clear();                    
                    FigureList.Menu();
                    break;
                case Select.Exit:
                    Environment.Exit(0);
                    break;
                default:
                    Program.ShowError();
                    SelectOption();
                    break;
            }
        }          
    }
}
