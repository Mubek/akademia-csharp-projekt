﻿namespace Projekt_Na_Zaliczenie
{
    public class Cube : Abstract3DFigure, ICalculate3D
    {
        public Cube(double a) : base(a)
        {
        }

        public double CalculateVolume()
        {
            return a * a * a;
        }

        public double CalculatePerimeter()
        {
            return 6 * (a * a);
        }
    }
}