﻿using System;

namespace Projekt_Na_Zaliczenie
{
    public static class GetFigurePoint
    {
        public static double a { get; private set; }
        public static double b { get; private set; }
        public static double h { get; private set; }
        public static double r { get; private set; }

        public static void GetA()
        {
            try
            {
                a = double.Parse(Console.ReadLine());
            }
            catch
            {
                Program.ShowError();
                GetA();
            }
        }

        public static void GetB()
        {
            try
            {
                b = double.Parse(Console.ReadLine());
            }
            catch
            {
                Program.ShowError();
                GetB();
            }
        }

        public static void GetH()
        {
            try
            {
                h = double.Parse(Console.ReadLine());
            }
            catch
            {
                Program.ShowError();
                GetH();
            }
        }

        public static void GetR()
        {
            try
            {
                r = double.Parse(Console.ReadLine());
            }
            catch 
            {
                Program.ShowError();
                GetR();
            }
        }
    }
}
