﻿namespace Projekt_Na_Zaliczenie
{
    public class Trapezoid : Abstract2DFigure ,ICalculate2D
    {
        private int beTwo = 2;

        public int Two
        {
            get { return beTwo; }

            set { Two = value; }
        }

        public Trapezoid(double a, double b, double h) : base(a, b, h)
        {
        }

        public double CalculateField()
        {
            return (a + b) / Two * h;
        }
    }
}