﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Projekt_Na_Zaliczenie
{
    public static class FigureList
    {
        private static readonly List<string> FiguresList = new List<string>()
        {
            "Square",
            "Rectangle",
            "Trapezoid",
            "Triangle",
            "Circle",
            "Ellipse"
        };
        private static string NewFigure { get; set; }

        public static void Menu()
        {
            string optionYesNo = "";

            ShowFigureList();
            for (;;)
            {
                Console.WriteLine("<Y/M> Do you want to update database of figures <Y> or back to menu? <M>");
                try
                {
                    optionYesNo = Console.ReadLine();
                }
                catch
                {
                    Program.ShowError();
                    Menu();
                }

                switch (optionYesNo)
                {
                    case "Y":
                    case "y":
                        UpdateFigureList();
                        break;
                    case "M":
                    case "m":
                        Program.ReturnToMenu();
                        break;
                    default:
                        Console.WriteLine("SAY Y OR M!!!");
                        break;
                }
            }
        }

        private static void ShowFigureList()
        {
            Console.WriteLine("Database of figures");
            foreach (var figure in FiguresList)
            {
                Console.WriteLine(figure);
            }
        }

        private static void UpdateFigureList()
        {
            GetNewFigure();
            bool isLetter = Regex.IsMatch(NewFigure, @"^[a-zA-Z]+$");

            if (isLetter)
            {
                FiguresList.Add(NewFigure);
                Console.Clear();
                Menu();
            }
            else
            {
                Console.WriteLine("Name can have only letters!\nTry again\n");
                UpdateFigureList();
            }
        }

        private static void GetNewFigure()
        {
            Console.WriteLine("Give a name of new figure and press Enter: ");
            try
            {
                NewFigure = Console.ReadLine();
            }
            catch
            {
                Program.ShowError();
                UpdateFigureList();
            }
        }       
    }
}

