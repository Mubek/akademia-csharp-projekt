﻿namespace Projekt_Na_Zaliczenie
{
    public class Square : Abstract2DFigure, ICalculate2D
    {
        public Square(double a) : base(a)
        {
        }

        public double CalculateField()
        {
            return a * a;
        }
    }
}