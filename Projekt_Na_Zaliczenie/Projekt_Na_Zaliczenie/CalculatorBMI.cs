﻿using System;

namespace Projekt_Na_Zaliczenie
{
    public static class CalculatorBMI
    {
        private static double Weight { get; set; }
        private static double Height { get; set; }
        private static double YourBMI { get; set; }

        public static void Menu()
        {
            GetWeight();
            GetHeight(); 
                      
            CalculateAndProcessBMI();
            ShowCategoryBMI();
        }

        private static void GetWeight()
        {
            Console.Write("Give your weight in kg: ");
            try
            {
                Weight = double.Parse(Console.ReadLine());
            }
            catch
            {
                Program.ShowError();
                GetWeight();
            }
            finally
            {
                if (Weight <= 0)
                {
                    Program.ShowError();
                    GetWeight();
                }
            }
        }

        private static void GetHeight()
        {

            Console.Write("Give your height in cm: ");
            try
            {
                Height = double.Parse(Console.ReadLine());
            }
            catch
            {
                Program.ShowError();
                GetHeight();
            }
            finally
            {
                if (Height <= 0)
                {
                    Program.ShowError();
                    GetHeight();
                }
            }
        }

        private static void CalculateAndProcessBMI()
        {
            Height = Height / 100;
            YourBMI = Weight / (Height * Height);
            YourBMI = (int)(YourBMI * 1000.0) / 1000.0;
        }

        private static void ShowCategoryBMI()
        {
            if (YourBMI < 18.5)
            {
                Console.WriteLine("Your BMI is {0} and you are underweight", YourBMI);
            }
            else
            {
                if (YourBMI >= 18.5 && YourBMI <= 24.99)
                {
                    Console.WriteLine("Your BMI is {0} and you are normal", YourBMI);
                }
                else if (YourBMI >= 25.0)
                {
                    Console.WriteLine("Your BMI is {0} and you are overweight", YourBMI);
                }
            }

            NextCalculate();
        }

        private static void NextCalculate()
        {
            string optionYesNo = "";

            Console.WriteLine("<Y/M> Do you want to calculate BMI again <Y>, or maybe back to menu? <M>");

            try
            {
                optionYesNo = Console.ReadLine();
            }
            catch
            {
                Program.ShowError();
                NextCalculate();
            }

            switch (optionYesNo)
            {
                case "Y":
                case "y":
                    Console.Clear();
                    Menu();
                    break;
                case "M":
                case "m":
                    Program.ReturnToMenu();
                    break;
                default:
                    Console.WriteLine("\nSAY Y OR M!!!");
                    NextCalculate();
                    break;
            }
        }
    }
}
