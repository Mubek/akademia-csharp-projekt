﻿namespace Projekt_Na_Zaliczenie
{
    public class Rectangle : Abstract2DFigure, ICalculate2D
    {
        public Rectangle(double a, double b) : base(a, b)
        {
        }

        public double CalculateField()
        {
            return a * b;
        }
    }
}
