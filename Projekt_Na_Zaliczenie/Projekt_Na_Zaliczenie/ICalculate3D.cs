﻿namespace Projekt_Na_Zaliczenie
{
    public interface ICalculate3D
    {
        double CalculateVolume();
        double CalculatePerimeter();
    }
}