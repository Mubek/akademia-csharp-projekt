﻿namespace Projekt_Na_Zaliczenie
{
    public abstract class Abstract2DFigure
    {
        public double a { get; set; }
        public double b { get; set; }
        public double h { get; set; }

        protected Abstract2DFigure(double a)
        {
            this.a = a;
        }

        protected Abstract2DFigure(double a, double b)
        {
            this.a = a;
            this.b = b;
        }

        protected Abstract2DFigure(double a, double b, double h)
        {
            this.a = a;
            this.b = b;
            this.h = h;
        }
    }
}
