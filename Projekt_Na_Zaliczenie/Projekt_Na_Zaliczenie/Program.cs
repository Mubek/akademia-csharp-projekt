﻿using System;

namespace Projekt_Na_Zaliczenie
{
    public class Program
    {                      
        private static void Main()
        {           
            MainMenu.Menu();            

            Console.ReadKey();
        }

        public static void ShowError()
        {
            Console.WriteLine("An error occurred");
            Console.WriteLine("Try again.\n");
        }

        public static void ReturnToMenu()
        {            
            Console.Clear();
            MainMenu.Menu();
        }
    }
}
