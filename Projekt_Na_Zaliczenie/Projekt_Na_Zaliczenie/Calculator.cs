﻿using System;

namespace Projekt_Na_Zaliczenie
{
    public static class Calculator
    {
        private static double a { get; set; }
        private static double b { get; set; }
        private static double Sum { get; set; }

        private enum Select
        {
            Add = 1,
            Subtract,
            Multiply,
            Divide
        }

        public static void Menu()
        {
            GetFirstValue();
            GetSecondValue();
            MenuText();
            SelectOption();
        }

        private static void GetFirstValue()
        {
            try
            {
                Console.Write("Give first value: ");
                a = double.Parse(Console.ReadLine());
            }
            catch
            {
                Program.ShowError();
                GetFirstValue();
            }
        }

        private static void GetSecondValue()
        {
            try
            {
                Console.Write("Give second value: ");
                b = double.Parse(Console.ReadLine());
            }
            catch
            {
                Program.ShowError();
                GetSecondValue();
            }
        }

        private static void MenuText()
        {
            Console.WriteLine("What do you want to do with this values?");
            Console.WriteLine("1. Add");
            Console.WriteLine("2. Subtract");
            Console.WriteLine("3. Multiply");
            Console.WriteLine("4. Divide");
        }

        private static void SelectOption()
        {
            Select option = 0;

            Console.Write("Choice: ");
            try
            {
                option = (Select)int.Parse(Console.ReadLine());
            }
            catch
            {
                Program.ShowError();
                SelectOption();
            }

            switch (option)
            {
                case Select.Add:
                    Console.WriteLine("Result: {0} \n", Add());
                    Sum = Add();
                    break;
                case Select.Subtract:
                    Console.WriteLine("Result: {0} \n", Subtract());
                    Sum = Subtract();
                    break;
                case Select.Multiply:
                    Console.WriteLine("Result: {0} \n", Multiply());
                    Sum = Multiply();
                    break;
                case Select.Divide:
                    Console.WriteLine("Result: {0} \n", Divide());
                    Sum = Divide();
                    break;
                default:
                    Program.ShowError();
                    SelectOption();
                    break;
            }
            NextCalculate();
        }

        private static void NextCalculate()
        {
            string optionYesNo = "";

            Console.WriteLine("<Y/N/M> Do you want to do next calculations on this value <Y>, " +
                              "back to calculator <N> or maybe back to menu? <M>");
            try
            {
                optionYesNo = Console.ReadLine();
            }
            catch
            {
                Program.ShowError();
                NextCalculate();
            }

            switch (optionYesNo)
            {
                case "Y":
                case "y":
                    a = Sum;
                    Console.Clear();
                    Console.WriteLine("Your previous result is {0}", Sum );
                    GetSecondValue();
                    MenuText();
                    SelectOption();
                    break;
                case "N":
                case "n":
                    Console.Clear();
                    Menu();
                    break;
                case "M":
                case "m":
                    Program.ReturnToMenu();
                    break;
                default:
                    Console.WriteLine("\nSAY Y, N OR M!");
                    NextCalculate();
                    break;
            }
        }
        
        private static double Add()
        {
            return a + b;
        }

        private static double Subtract()
        {
            return a - b;
        }

        private static double Multiply()
        {
            return a * b;
        }

        private static double Divide()
        {
            return a / b;
        }
    }
}
