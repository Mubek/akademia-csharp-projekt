﻿namespace Projekt_Na_Zaliczenie
{
    public abstract class Abstract3DFigure
    {
        public double a { get; set; }
        public double r { get; set; }
        public double h { get; set; }
        public const double pi = 3.14;

        protected Abstract3DFigure(double a)
        {
            this.a = a;
        }

        protected Abstract3DFigure(double r, double h)
        {
            this.r = r;
            this.h = h;
        }

        protected Abstract3DFigure(double a, double r, double h)
        {
            this.a = a;
            this.r = r;
            this.h = h;
        }
    }
}