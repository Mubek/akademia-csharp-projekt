﻿namespace Projekt_Na_Zaliczenie
{
    public class Cylinder : Abstract3DFigure
    {
        public Cylinder(double r, double h) : base(r, h)
        {
        }

        public double CalculateVolume()
        {
            return pi * (r * r) * h;
        }

        public double CalculatePerimeter()
        {
            return 2 * pi * r * (r + h);
        }
    }
}